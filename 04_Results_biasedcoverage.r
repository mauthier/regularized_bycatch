##--------------------------------------------------------------------------------------------------------
## SCRIPT : simulation Common Dolphins - results of balanced sampling
##
## Authors : Matthieu Authier
## Last update : 2020-12-03
## R version 4.0.2 (2020-06-22) -- "Taking Off Again"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("lubridate", "skimr", "tidyverse", "rstan"), library, character.only = TRUE)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

rm(list = ls())

setwd(WorkDir <- "C:/Users/mauthier/Desktop/MrP_simul")
DataDir <- paste(WorkDir, "data", sep = "/")
OutDir <- paste(WorkDir, "output", sep = "/")

load(paste(OutDir, "1_year_biased.RData", sep = "/"))

### bias
theta <- theta %>% 
  mutate(bias = 100 * round((N_bycatch - theta) / N_bycatch, 3),
         inside = ifelse(theta > lower, 1, 0) * ifelse(theta < upper, 1, 0)
         )

theme_set(theme_bw())
theta %>% 
  ggplot(aes(x = method, y = bias)) +
  geom_violin() +
  scale_y_continuous(name = "Bias (%)") +
  theme(legend.position = "top", 
        legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text = element_text(size = 12)
        )
ggsave(filename = paste(OutDir, "1_year_biased_boxplot.png", sep = "/"), 
       width = 20, height = 15, units = "cm", dpi = 600
       )


theta %>% 
  ggplot(aes(x = N_bycatch, y = theta, color = method, group = method)) +
  geom_abline(intercept = 0, slope = 1, color = "tomato", linetype = "dotted") +
  geom_point(alpha = 0.1) +
  geom_smooth(method = "lm", se = FALSE) +
  # scale_x_log10(name = "Truth", breaks = seq(0, 500, 50)) +
  # scale_y_log10(name = "Estimate", breaks = seq(0, 1000, 100)) +
  scale_x_continuous(name = "Truth", breaks = seq(0, 500, 50)) +
  scale_y_continuous(name = "Estimate", breaks = seq(0, 1000, 100)) +
  scale_color_brewer(name = "Method", type = "qual", palette = "Set2") +
  theme(legend.position = "top", 
        legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text = element_text(size = 12)
        )
# ggsave(filename = paste(OutDir, "1_year_biased_calibration_log.png", sep = "/"),
#        width = 20, height = 15, units = "cm", dpi = 600
#        )
ggsave(filename = paste(OutDir, "1_year_biased_calibration.png", sep = "/"),
       width = 20, height = 15, units = "cm", dpi = 600
       )
### look at the temporal pattern
head(mrp)

mrp %>% 
  mutate(theta = theta / n_op) %>% 
  ggplot(aes(x = week_id, y = theta, group = sim)) +
  geom_line(alpha = 0.05) +
  scale_y_continuous(name = "Bycatch risk") +
  scale_x_continuous(name = "Week", breaks = seq(1, 28, 4)) +
  theme(legend.position = "top", 
        legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text = element_text(size = 12)
        )
ggsave(filename = paste(OutDir, "1_year_biased_risk.png", sep = "/"), 
       width = 20, height = 15, units = "cm", dpi = 600
       )

### observed events
theta %>% 
  ggplot(aes(x = bycatch_obs, y = bias, group = method, color = method)) +
  geom_point(alpha = 0.1) +
  geom_smooth(method = "lm", se = FALSE) +
  scale_y_continuous(name = "Bias (%)") +
  scale_x_sqrt(name = "Observed events", breaks = c(1, seq(5, 60, 5))) +
  theme(legend.position = "top", 
        legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text = element_text(size = 12)
        )
ggsave(filename = paste(OutDir, "1_year_biased_observed_events.png", sep = "/"), 
       width = 20, height = 15, units = "cm", dpi = 600
       )
