MrP_code <- '
data {
 int<lower = 1> n_obs;
 int<lower = 1> n_week;
 int<lower = 1> n_year;
 int<lower = 1> n_vessel;
 int<lower = 1, upper = n_week> WEEK[n_obs];    // ID of week
 int<lower = 1, upper = n_vessel> VESSEL[n_obs];// ID of vessel
 int<lower = 1, upper = n_year> YEAR[n_obs];  // ID of year
 int<lower = 0> BYCATCH[n_obs];
 int<lower = 1> N[n_obs];
 real<lower = 0.0> prior_scale;
 real prior_location_intercept;
 real<lower = 0.0> prior_scale_intercept;
 vector[n_week] n_op[n_year];
 corr_matrix[n_week] R; // correlation matrix for intra-year level variations
}

parameters {
  real u_intercept;
  real<lower = 0.0> tau;
  real<lower = 0.0> psi;
  simplex[3] prop;
  vector[n_week] u_alpha;  // unscaled week effects
  vector[n_week] u_delta[n_year];// unscaled year effects
  vector[n_vessel] u_nu;// unscaled vessel effects
}

transformed parameters {
  vector[n_obs] linpred; // linear predictor
  real intercept;
  vector[n_week] alpha;  // week effects
  vector[n_week] delta[n_year];// year effects
  vector[n_vessel] nu;   // vessel effects
  vector[3] sigma;       // variability
  intercept = prior_location_intercept + prior_scale_intercept * u_intercept;
  sigma = prior_scale * sqrt(prop * psi / tau);
  alpha =  intercept + sigma[1] * cumulative_sum(u_alpha);
  for(k in 1:n_year) {
    delta[k] = alpha + sigma[2] * u_delta[k];
  }
  nu =  sigma[3] * u_nu;
  for(i in 1:n_obs) {
  	linpred[i] = delta[YEAR[i], WEEK[i]] + nu[VESSEL[i]];
  }
}

model {
  u_intercept ~ normal(0.0, 1.0);
  u_alpha ~ normal(0.0, 1.0);
  u_nu ~ normal(0.0, 1.0);
  for(k in 1:n_year) {
    u_delta[k] ~ multi_normal(rep_vector(0.0, n_week), R);
  }
  tau ~ gamma(0.5, 1.0);
  psi ~ gamma(0.5, 1.0);
  BYCATCH ~ binomial_logit(N, linpred);
}

generated quantities {
  real log_lik[n_obs];
  int<lower = 0> bycatch_rep[n_obs];
  vector[n_obs] time_rep;
  vector[n_week] risk[n_year];
  vector[n_week] bycatch_hat[n_year];
  vector[n_year] bycatch_tot;
  real new_vessel;
  new_vessel = normal_rng(0.0, 1.0) * sigma[3];
  for(i in 1:n_obs) {
    log_lik[i] = binomial_logit_lpmf(BYCATCH[i]| N[i], linpred[i]); // for model selection
    bycatch_rep[i] = binomial_rng(N[i], inv_logit(linpred[i])); // posterior predictive check
  }
  for(k in 1:n_year) {
  	risk[k] = inv_logit(delta[k] + rep_vector(new_vessel, n_week));
    bycatch_hat[k] = n_op[k] .* risk[k];
    bycatch_tot[k] = sum(bycatch_hat[k]);	
  }
}
'

get_summary <- function(x, alpha = 0.2) {
  x <- as.numeric(x)
  return(c(median(x), coda::HPDinterval(coda::as.mcmc(x), prob = 1 - alpha)))
}

matern_cov <- function(d, sill = 1, range = 1) {
  sill * sill * (1.0 + abs(d) * sqrt(3.0) / range) * exp(-abs(d) * sqrt(3.0) / range)
}

week <- 1:52
R <- matrix(0, nrow = 52, ncol = 52)
# R is the correlation matrix between weeks, chosen so that correlation is 0.05 after 4 weeks

R[1, ] <- 1:52 - 1
R[, 1] <- R[1, ]

for(i in 2:(nrow(R) - 1)) {
  R[i, (i + 1):nrow(R)] <- week[1:length((i + 1):nrow(R))]
  R[(i + 1):nrow(R), i] <- R[i, (i + 1):nrow(R)]
}; rm(i, week)
R <- matern_cov(d = R, range = 1.5) # R is now a correlation matrix
