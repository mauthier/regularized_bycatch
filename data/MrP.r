MrP_code <- '
data {
 int<lower = 1> n_obs;
 int<lower = 1> n_week;
 int<lower = 1> n_vessel;
 int<lower = 1, upper = n_week> WEEK[n_obs];    // ID of week
 int<lower = 1, upper = n_vessel> VESSEL[n_obs];// ID of vessel
 int<lower = 0> BYCATCH[n_obs];
 int<lower = 1> N[n_obs];
 real<lower = 0.0> prior_scale;
 real prior_location_intercept;
 real<lower = 0.0> prior_scale_intercept;
 vector[n_week] n_op;
}

parameters {
  real u_intercept;
  real<lower = 0.0> tau;
  real<lower = 0.0> psi;
  simplex[2] prop;
  vector[n_week] u_alpha;  // unscaled week effects
  vector[n_vessel] u_delta;// unscaled vessel effects
}

transformed parameters {
  vector[n_obs] linpred; // linear predictor
  real intercept;
  vector[n_week] alpha;  // week effects
  vector[n_vessel] delta;// vessel effects
  vector[2] sigma;         // variability
  intercept = prior_location_intercept + prior_scale_intercept * u_intercept;
  sigma = prior_scale * sqrt(prop * psi / tau);
  alpha =  intercept + sigma[1] * cumulative_sum(u_alpha);
  delta =  sigma[2] * u_delta;
  linpred = alpha[WEEK] + delta[VESSEL];
}

model {
  u_intercept ~ normal(0.0, 1.0);
  u_alpha ~ normal(0.0, 1.0);
  u_delta ~ normal(0.0, 1.0);
  tau ~ gamma(0.5, 1.0);
  psi ~ gamma(0.5, 1.0);
  BYCATCH ~ binomial_logit(N, linpred);
}

generated quantities {
  real log_lik[n_obs];
  int<lower = 0> bycatch_rep[n_obs];
  vector[n_obs] time_rep;
  vector[n_week] risk[2];
  vector[n_week] bycatch_hat[2];
  real bycatch_tot[2];
  real new_vessel;
  new_vessel = normal_rng(0.0, 1.0) * sigma[2];
  for(i in 1:n_obs) {
    log_lik[i] = binomial_logit_lpmf(BYCATCH[i]| N[i], linpred[i]); // for model selection
    bycatch_rep[i] = binomial_rng(N[i], inv_logit(linpred[i])); // posterior predictive check
  }
  risk[1] = inv_logit(alpha);
  bycatch_hat[1] = n_op .* risk[1];
  bycatch_tot[1] = sum(bycatch_hat[1]);
  risk[2] = inv_logit(rep_vector(new_vessel, n_week) + alpha);
  bycatch_hat[2] = n_op .* risk[2];
  bycatch_tot[2] = sum(bycatch_hat[2]);
}
'

get_summary <- function(x, alpha = 0.2) {
  x <- as.numeric(x)
  return(c(median(x), coda::HPDinterval(coda::as.mcmc(x), prob = 1 - alpha)))
}
