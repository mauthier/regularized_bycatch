##--------------------------------------------------------------------------------------------------------
## SCRIPT : Simulate biased sampling of Obsmer
##
## Authors : Matthieu Authier
## Last update : 2020-12-02
## R version 4.0.1 (2020-06-06) -- "See Things Now"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("lubridate", "skimr", "tidyverse", "rstan"), library, character.only = TRUE)

# rm(list = ls())

accept_observer <- function(n_week = 52,
                            b0 = log(5) / 5, 
                            b1 = log(5) / 10,
                            coverage = 0.1,
                            reverse = FALSE # to simulate oversampling instead of undersampling
                            ) {
  x <- 1:26
  if(reverse) { x <- rev(x) }
  a <- runif(1, 0.01, 0.10)
  x0 <- sample(5:8, size = 1)
  delta <- 5 + rnorm(1)
  y <- log(a / (1 - a)) + b0 * (x - x0) + (b1 - b0) * delta * log(1 + exp((x - x0) / delta))
  if(n_week == 53) {
    y <- plogis(c(y, rev(y), y[1]))
  } else {
    y <- plogis(c(y, rev(y)))
  }
  y <- y / mean(y) * coverage
  return(y)
}

# plot(1:52, accept_observer(reverse = TRUE), type = 'l', las = 1)
# plot(c(1, 52), c(0, 1),
#      type = 'n', bty = 'n', las = 1, ylab = "Observer onboard", xlab = "week")
# for(i in 1:100) lines(1:52, accept_observer(reverse = TRUE), col = grey(0.9))
# rm(i)

dolphin <- function(x0 = 70, 
                    x1 = 20, 
                    a = 0.8, 
                    b0 = -log(2) / 20, 
                    b1 = -log(10) / 10, 
                    b2 = -log(10) / 5, 
                    delta = 15,
                    random = TRUE,
                    leap = FALSE
                    ) {
  x = 1:183
  eps1 <- drop(diag(c(log(2) / 2, 7.5)) %*% t(chol(matrix(c(1, -0.8, -0.8, 1), nrow = 2))) %*% rnorm(2))
  eps2 <- drop(diag(c(0.05, 2.5)) %*% t(chol(matrix(c(1, -0.7, -0.7, 1), nrow = 2))) %*% rnorm(2))
  a <- log(a / (1 - a)) + ifelse(random, 1, 0) * eps1[1]
  x0 <- x0 + ifelse(random, 1, 0) * eps1[2]
  b1 <- b1 + ifelse(random, 1, 0) * eps2[1]
  b2 <- b2 + ifelse(random, 1, 0) * eps2[1]
  delta <- abs(delta + ifelse(random, 1, 0) * eps2[2])
  if(random) {
    ups <- c(0.4, 1.2)
  } else {
    ups <- c(runif(1, 0.2, 0.6), runif(1, 1.1, 1.3))
  }
  y1 <- a + b0 * (x - x0) + (b1 - b0) * delta * log(1 + exp((x - x0) / delta))
  if(leap) {
    y2 <- a * ups[2] + b0 * (x - x1) + (b1 - b0) * (delta * ups[1]) * log(1 + exp((x - x1) / (delta * ups[1])))
  } else {
    y2 <- a * ups[2] + b0 * (x[-183] - x1) + (b1 - b0) * (delta * ups[1]) * log(1 + exp((x[-183] - x1) / (delta * ups[1])))
  }
  y <- plogis(c(y1, rev(y2)))
  return(y)
}

# plot(c(1, 365), c(0, 1),
#      type = 'n', bty = 'n', las = 1, ylab = "Dolphin presence", xlab = "day")
# for(i in 1:100) lines(1:365, dolphin(), col = grey(0.9))
# lines(1:365, dolphin(random = FALSE), col = "midnightblue")
# abline(v = yday(ymd(c("2019-03-15", "2019-12-10"))))
# rm(i)

ptm <- function(N = 20,
                year = 2019,
                activity = 0.8,
                risk_given_presence = 0.3,
                heterogeneity = log(2) / 3,
                coverage = 0.05,
                biased = FALSE,
                oversampling = FALSE,
                repro_id = 20201201
                ) {
  ### is biased is true, than coverage is twice the nominal value, ie emulate oversampling but at the wrong time
  set.seed(seed = repro_id)
  
  begin_date = paste(year, "01-01", sep = "-")
  end_date = paste(year, "12-31", sep = "-")
  n_week <- week(end_date)
  
  ### days at sea
  DaS <- data.frame(date = ymd(begin_date) + days(yday(ymd(begin_date)):yday(ymd(end_date)) - 1)) %>%
    mutate(day = yday(date),
           dolphin_presence = dolphin(leap = leap_year(begin_date)),
           risk = risk_given_presence * dolphin_presence,
           weekday = wday(date)
           ) %>%
    filter(weekday %in% c(7, 1:4)) %>%
    mutate(week_id = week(date)) %>%
    as.data.frame()
  
  ### activity matrix of PTM
  p <- plogis(log(activity/(1 - activity)) + log(1.2) * cumsum(rnorm(n_week)))
  active_ptm <- t(replicate(N, rbinom(n = n_week, size = 1, prob = p)))
  vessel_risk <- rnorm(N) * heterogeneity
  rownames(active_ptm) <- as.character(1:nrow(active_ptm))
  colnames(active_ptm) <- as.character(1:n_week)
  ptm <- data.frame(week_id = 1:n_week,
                    n_ptm = apply(active_ptm, 2, sum)
                    )
  DaS <- DaS %>%
    left_join(y = ptm,
              by = "week_id"
              )
  
  ### fishing
  fishing <- lapply(1:N, function(vessel) {
    DaS %>%
      filter(week_id %in% which(active_ptm[vessel, ] == 1)) %>%
      mutate(n_OP = sample.int(n = 3, size = n(), prob = c(1 / 24, 15 / 24, 8 / 24), replace = TRUE),
             risk = plogis(log(risk / (1 - risk)) + vessel_risk[vessel]),
             n_bycatch = rbinom(n = n_OP, size = 1, prob = risk)
             ) %>%
      select(date, week_id, n_OP, n_bycatch) %>%
      mutate(ptm_ID = vessel)
  })
  
  ### summary stats
  sumstat <- do.call('rbind', fishing) %>% 
    group_by(week_id) %>% 
    summarize(n_bycatch = sum(n_bycatch),
              n_op = sum(n_OP)
              )
  
  ### observations
  onboard <- accept_observer(n_week = n_week, coverage = 2 * coverage, reverse = oversampling)
  ## coverage
  ptm <- ptm %>%
    # left_join(y = DaS %>%
    #             group_by(week_id) %>%
    #             summarize(proba = (1 - mean(dolphin_presence))) %>%
    #             mutate(proba = log(proba / (1 - proba)),
    #                    proba = (proba - mean(proba)) / sd(proba),
    #                    proba = plogis(log(coverage/(1 - coverage)) + log(2) / 2 * proba),
    #                    proba = ifelse(biased, 1, 0) * proba + ifelse(biased, 0, 1) * coverage
    #                    ),
    #           by = "week_id"
    #           ) %>%
    mutate(proba = onboard[week_id],
           ### is sampling biased?
           proba = ifelse(biased, 1, 0) * proba + ifelse(biased, 0, 1) * coverage,
           n_observer = rbinom(n = n(), size = n_ptm, prob = proba)
           )
  
  ## sample vessel
  obsmer <- ptm %>%
    filter(n_observer != 0) %>%
    select(week_id, n_observer)
  
  bycatch_data <- lapply(1:nrow(obsmer), function(id) {
    who <- sample(as.numeric(which(active_ptm[, obsmer[id, "week_id"]] != 0)), 
                  size = obsmer[id, "n_observer"]
                  )
    what <- lapply(who, function(vessel) {
      fishing[[vessel]] %>%
        filter(week_id %in% obsmer[id, "week_id"]) %>%
        select(date, ptm_ID, n_OP, n_bycatch) %>%
        as.data.frame()
    })
    return(do.call('rbind', what))
  } )
  bycatch_data <- do.call('rbind', bycatch_data)
  
  ### wgbyc computations
  wgbyc <- bycatch_data %>% 
    summarize(N_bycatch = sum(n_bycatch),
              N_OP = sum(n_OP)
              ) %>% 
    mutate(theta = N_bycatch / N_OP,
           lower_clopperpearson = qbeta(0.05 / 2, shape1 = N_bycatch, shape2 = N_OP - N_bycatch + 1),
           upper_clopperpearson = qbeta(1 - 0.05 / 2, shape1 = N_bycatch + 1, shape2 = N_OP - N_bycatch)
           )
  boot <- replicate(1e4, sample.int(n = nrow(bycatch_data), size = nrow(bycatch_data), replace = TRUE)) %>% 
    apply(MARGIN = 2, function(x) {
      df <- bycatch_data[x, ]
      return(sum(df$n_bycatch) / sum(df$n_OP))
    })
  wgbyc <- wgbyc %>% 
    mutate(lower_boot = as.numeric(quantile(boot, probs = 0.025)) - (theta - mean(boot)),
           upper_boot = as.numeric(quantile(boot, probs = 0.975)) - (theta - mean(boot))
           ) %>% 
    as.data.frame()
  wgbyc <- rbind(wgbyc,
                 data.frame(N_bycatch = sum(sumstat$n_bycatch),
                            N_OP = sum(sumstat$n_op)
                            ) %>% 
                   mutate(theta = wgbyc$theta * N_OP,
                          lower_clopperpearson = wgbyc$lower_clopperpearson * N_OP,
                          upper_clopperpearson = wgbyc$upper_clopperpearson * N_OP,
                          lower_boot = wgbyc$lower_boot * N_OP,
                          upper_boot = wgbyc$upper_boot * N_OP
                          ) %>% 
                   as.data.frame()
                 )
  wgbyc$what <- c('rate', 'abundance')
  
  ### wrap up
  return(list(active_ptm = active_ptm,
              DaS = DaS,
              fishing = fishing,
              obsmer = obsmer,
              bycatch_data = bycatch_data,
              sumstat = sumstat,
              wgbyc = wgbyc
              )
         )
}

### test
# dd <- ptm(repro_id = 123)
# skim(dd$bycatch_data)
# 
# dd$bycatch_data %>%
#   mutate(week_id = week(date)) %>%
#   group_by(week_id) %>%
#   summarize(risk = mean(n_bycatch / n_OP)) %>%
#   ggplot(aes(x = week_id, y = risk)) +
#   geom_line() +
#   theme_bw()
# 
# dd <- ptm(biased = TRUE, repro_id = 123)
# skim(dd$bycatch_data)
# 
# dd$bycatch_data %>%
#   mutate(week_id = week(date)) %>%
#   group_by(week_id) %>%
#   summarize(risk = mean(n_bycatch / n_OP)) %>%
#   ggplot(aes(x = week_id, y = risk)) +
#   geom_line() +
#   theme_bw()
