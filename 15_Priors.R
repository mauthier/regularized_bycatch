##--------------------------------------------------------------------------------------------------------
## SCRIPT : Priors
##
## Authors : Matthieu Authier
## Last update : 2021-04-13
## R version 4.0.4 (2021-02-15) -- "Lost Library Book"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "viridisLite"), library, character.only = TRUE)

rm(list = ls())

matern_cov <- function(d, sill = 1, range = 1) {
  sill * sill * (1.0 + abs(d) * sqrt(3.0) / range) * exp(-abs(d) * sqrt(3.0) / range)
}

week <- 1:52
R <- matrix(0, nrow = 52, ncol = 52)
# R is the correlation matrix between weeks, chosen so that correlation is 0.05 after 4 weeks

R[1, ] <- 1:52 - 1
R[, 1] <- R[1, ]

for(i in 2:(nrow(R) - 1)) {
  R[i, (i + 1):nrow(R)] <- week[1:length((i + 1):nrow(R))]
  R[(i + 1):nrow(R), i] <- R[i, (i + 1):nrow(R)]
}; rm(i, week)

R <- matern_cov(d = R, range = 1.5) # R is now a correlation matrix
Omega <- t(chol(R))

### reproducibility
set.seed(20210413)

### intercept
n_sim <- 1e3
mu <- 1.5 * rnorm(n_sim)

### prop
prop <- MCMCpack::rdirichlet(n_sim, alpha = rep(1, 3))

### sd
prior_scale <- log(10) / 2
tau <- rgamma(n_sim, 0.5, 0.5)
sigma2 <- rgamma(n_sim, 0.5, 0.5)
sigma <- prior_scale * sqrt(sigma2 / tau)

sigma_vessel <- sqrt(prop[, 1]) * sigma
sigma_week <- sqrt(prop[, 2]) * sigma
sigma_year <- sqrt(prop[, 3]) * sigma 

### random walk
epsilon <- matrix(rep(mu, each = 52), nrow = 52, byrow = FALSE) + 
  apply(replicate(n = 52, rnorm(n_sim)), 1, cumsum) %*% diag(sigma_week)

### GP
beta <- epsilon +
  Omega %*% replicate(n = n_sim, rnorm(52)) %*% diag(sigma_year)

### bycatch risk
risk <- plogis(beta) %>% 
  as.data.frame()

names(risk) <- as.character(1:n_sim)

risk <- risk %>% 
  mutate(week = 1:52) %>% 
  pivot_longer(cols = -week,
               names_to = "ID"
               ) %>% 
  mutate(ID = as.numeric(ID))

subsample <- sample.int(3e1, replace = FALSE)

theme_set(theme_bw(base_size = 12))
risk %>% 
  filter(ID %in% subsample) %>% 
  ggplot(aes(x = week, y = value, group = ID)) +
  geom_path(alpha = 0.3, color = "midnightblue") +
  scale_x_continuous(name = "Week", breaks = c(1, seq(4, 50, 4), 52)) +
  scale_y_continuous(name = "Bycatch risk", breaks = seq(0, 1, 0.2)) +
  theme(legend.position = "top", 
        legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text.x = element_text(size = 8), 
        axis.text.y = element_text(size = 10)
        )

ggsave(filename = paste("output", "Figure2.jpeg", sep = "/"), 
       width = 15, height = 12, units = "cm", dpi = 300
       )
