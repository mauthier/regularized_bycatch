##--------------------------------------------------------------------------------------------------------
## SCRIPT : Figure 1
##
## Authors : Matthieu Authier
## Last update : 2021-03-22
## R version 4.0.2 (2020-06-22) -- "Taking Off Again"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("lubridate", "skimr", "tidyverse", "rstan"), library, character.only = TRUE)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

rm(list = ls())

source(paste("simul", "data", "simul_dolphin_multiyear.r", sep = "/"))

set.seed(123)
dd <- rbind(data.frame(x = rep(1:365, times = 3),
                       y = rep(0.3, 365 * 3),
                       iter = rep(1, 365 * 3),
                       trans = rep(1, 365 * 3),
                       param = rep("bycatch probability\nif dolphin present", 365 * 3),
                       scenario = rep(c("unbiased\nsampling", "under-\nsampling", "over-\nsampling"), each = 365)
                       ),
            data.frame(x = rep(1:365, times = 3 * 100),
                       y = do.call('c', lapply(1:300, function(i) { dolphin() })),
                       iter = rep(1:300, each = 365),
                       trans = rep(0.1, 365 * 300),
                       param = rep("dolphin\npresence", 3 * 365 * 100),
                       scenario = rep(c("unbiased\nsampling", "under-\nsampling", "over-\nsampling"), each = 2 * 365)
                       ),
            data.frame(x = rep(7 * c(0:52) + 1, times = 1+ 100 * 2),
                       y = c(rep(0.05, 53),
                             do.call('c', lapply(1:100, function(i) { c(0, accept_observer()) })),
                             do.call('c', lapply(1:100, function(i) { c(0, accept_observer(reverse = TRUE)) }))
                             ),
                       iter = c(rep(1, 53), rep(1:100, each = 53, times = 2)),
                       trans = c(rep(1, 53), rep(0.1, 53 * 100 * 2)),
                       param = rep("Observer\nonboard", 53 * (100 * 2 + 1)),
                       scenario = c(rep("unbiased\nsampling", 53), 
                                    rep("under-\nsampling", 100 * 53), 
                                    rep("over-\nsampling", 100 * 53)
                                    )
                       )
            )

theme_set(theme_bw(base_size = 12))
dd %>% 
  mutate(param = factor(param, levels = c("bycatch probability\nif dolphin present",
                                          "dolphin\npresence",
                                          "Observer\nonboard")
                        ),
         scenario = factor(scenario, 
                           levels = c("unbiased\nsampling", 
                                      "under-\nsampling", 
                                      "over-\nsampling"
                                      )
                           ),
         y = ifelse(x == 1, NA, y)
         ) %>% 
  ggplot() +
  geom_line(aes(x = x, y = y, group = iter, alpha = trans), color = "midnightblue") +
  scale_y_sqrt(name = "",
               breaks = c(0.005, 0.05, 0.3, 0.8, 1.0)
               ) +
  scale_x_continuous(name = "Week",
                     breaks = seq(1, 365, 4 * 7),
                     labels = seq(1, 53, 4)
                     ) +
  facet_grid(param ~ scenario) +
  guides(alpha = "none") +
  theme(legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text.x = element_text(size = 8, angle = 45), 
        axis.text.y = element_text(size = 10)
        )

ggsave(filename = paste("simul", "output", "Figure1.jpeg", sep = "/"),
       width = 15, height = 12, dpi = 300, units = "cm"
       )
