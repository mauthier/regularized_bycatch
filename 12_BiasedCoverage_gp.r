##--------------------------------------------------------------------------------------------------------
## SCRIPT : simulation Common Dolphins - balanced sampling
##
## Authors : Matthieu Authier
## Last update : 2021-03-15
## R version 4.0.2 (2020-06-22) -- "Taking Off Again"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("lubridate", "skimr", "tidyverse", "rstan"), library, character.only = TRUE)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

rm(list = ls())

source(paste("output", "simul_dolphin_multiyear.r", sep = "/"))

source(paste("output", "MrP2.r", sep = "/"))

## Compilation
M <- stan_model(model_code = MrP_code,
                model_name = "MrP model for bycatch estimation",
                verbose = TRUE
                )

### Modelling
n_chains <- 4
n_iter <- 1000
n_warm <- 500
n_thin <- 1

### monte carlo study
n_sim <- 1e2
scenario <- c(5, 10) # c(3, 15) # 
year <- 2004 + 1:15
n_year <- length(year)

# for reproducibility
set.seed(20201206)
seeds <- array(NA, dim = c(n_sim, n_year))
for(j in 1:n_year) {
  seeds[, j] <- sample(((j-1) * 1e5):((j * 1e5) - 1), size = n_sim, replace = FALSE)
}

for(j in scenario) {
  # store results
  risk <- mrp <- theta <- NULL

  for(i in 1:n_sim) {
    sumstat <- bycatch_data <- NULL
    for(k in 1:j) {
      dd <- ptm(repro_id = seeds[i, k], year = year[k], biased = TRUE)
      temp <- dd$bycatch_data %>% 
        mutate(week_id = week(date),
               YEAR = k,
               YEAR = ifelse(week_id == 53, YEAR + 1, YEAR),
               week_id = ifelse(week_id == 53, 1, week_id)
               )
      bycatch_data <- rbind(bycatch_data, temp)
      sumstat <- rbind(sumstat,
                       dd$sumstat %>% 
                         mutate(YEAR = k,
                                YEAR = ifelse(week_id == 53, YEAR + 1, YEAR),
                                week_id = ifelse(week_id == 53, 1, week_id)
                                )
                       )
      rm(temp, dd)
    }; gc()
    
    ### filter & aggregate
    bycatch_data <- bycatch_data %>% 
      filter(YEAR <= j)
    sumstat <- sumstat %>% 
      filter(YEAR <= j) %>% 
      group_by(YEAR, week_id) %>% 
      summarize(n_bycatch = sum(n_bycatch),
                n_op = sum(n_op)
                )
    n_op <- pivot_wider(sumstat, id_cols = c("week_id", "YEAR"), names_from = "YEAR", values_from = "n_op") %>% 
      select(-week_id) %>% 
      as.matrix() %>% 
      t()
    # data for stan
    standata <- with(bycatch_data,
                     list(n_obs = nrow(bycatch_data),
                          n_week = 52,
                          n_vessel = length(unique(ptm_ID)),
                          n_year = j,
                          WEEK = week_id,
                          VESSEL = as.numeric(factor(ptm_ID, levels = unique(ptm_ID))),
                          YEAR = YEAR,
                          BYCATCH = n_bycatch,
                          N = n_OP,
                          prior_scale = log(2) / 10,
                          prior_location_intercept = 0,
                          prior_scale_intercept = 1.5,
                          n_op = ifelse(is.na(n_op), 0, n_op),
                          R = R
                          )
                     )
    # Now model fitting but check first that there is data
    if(sum(standata$BYCATCH) != 0) {
      fit <- sampling(object = M,
                      data = standata, 
                      pars = c("bycatch_hat", "bycatch_tot", "alpha"),
                      chains = n_chains, 
                      iter = n_iter, 
                      warmup = n_warm, 
                      thin = n_thin,
                      control = list(adapt_delta = 0.99, max_treedepth = 15)
                      )
      
      # check convergence
      ggsave(paste(OutDir, "/convergence/", j,"_year_biased_conv_", i, ".png", sep = ""), 
             plot = plot(fit, plotfun = 'rhat'), 
             width = 10, height = 10, units = "cm"
             )
      
      # estimate bycatch
      df <- NULL
      for(k in 1:j) {
        df <- rbind(df, t(apply(rstan::extract(fit, 'bycatch_hat')$bycatch_hat[, k, ], 2, get_summary)))
      }
      df <- df %>% 
        round(3) %>% 
        as.data.frame()
      names(df) <- c("theta", "lower", "upper")
      df <- left_join(df %>% mutate(YEAR = rep(1:j, each = 52), week_id = rep(1:52, j)), 
                      sumstat, 
                      by = c("YEAR", "week_id")
                      ) %>%
            mutate(n_bycatch = ifelse(is.na(n_bycatch), 0, n_bycatch),
                   n_op = ifelse(is.na(n_op), 0, n_op),
                   sim = i
                   ) %>%
            select(YEAR, week_id, n_bycatch, n_op, theta, lower, upper, sim) %>%
            as.data.frame()

      mrp <- rbind(mrp, df)
      
      # risk profile
      df <- t(apply(plogis(rstan::extract(fit, 'alpha')$alpha), 2, get_summary)) %>% 
        round(4) %>% 
        as.data.frame()
      names(df) <- c("theta", "lower", "upper")
      risk <- rbind(risk, 
                    df %>% 
                      mutate(week_id = 1:52,
                             sim = i
                      )
                    )
      
      # total bycatch per year
      df <- t(apply(rstan::extract(fit, 'bycatch_tot')$bycatch_tot, 2, get_summary))%>% 
        round(0) %>% 
        as.data.frame()
      names(df) <- c("theta", "lower", "upper")
      
      theta <- rbind(theta, 
                     cbind(sumstat %>% 
                             group_by(YEAR) %>% 
                             summarize(n_OP = sum(n_op),
                                       truth = sum(n_bycatch)
                             ),
                           df
                           ) %>% 
                       mutate(sim = i)
                     )
      
      rm(fit, standata, df, sumstat, bycatch_data, n_op)
    }
    if(i %% 15 == 0) {
      save(list = c("risk", "mrp", "theta"), file = paste(OutDir, "/", j, "_year_biased.RData", sep = ""))
    }
  }
  gc()
  save(list = c("risk", "mrp", "theta"), file = paste(OutDir, "/", j, "_year_biased.RData", sep = ""))
}
