# Regularized Bycatch

Codes to implement regularized multilevel regression with post-stratification in the context of bycatch estimation.

All codes to reproduce the analyses described in the paper are provided. **Stan** code are the '\data' folder:
 - MrP.r for analyzing a single year of data, and
 
 - MrP2.r for analyzing several years of data

Custom functions for simulating data are the '\data' folder, in the file *simul_dolphin_multiyear.r*. The main functions are:

  - *accept_observer()*: simulate observers being allowed onboard fishing vessels at the week level;
  
  - *dolphin()*: simulate weekly dolphin presence during a year; and
  
  - *ptm()*: simulate bycatch in a hypothetical fishery, and simulate observed PETS bycatch data.