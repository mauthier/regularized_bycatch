##--------------------------------------------------------------------------------------------------------
## SCRIPT : Several years
##
## Authors : Matthieu Authier
## Last update : 2021-03-29
## R version 4.0.4 (2021-02-15) -- "Lost Library Book"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("lubridate", "skimr", "tidyverse", "table"), library, character.only = TRUE)

rm(list = ls())

coverage_year <- read.table(paste("output", "20210329_YEAR.txt", sep = "/"),
                            header = TRUE, 
                            colClasses = c(rep("numeric", 6), "character")
                            )

coverage_week <- read.table(paste("output", "20210329_WEEK.txt", sep = "/"),
                            header = TRUE, 
                            colClasses = c(rep("numeric", 8), "character")
                            )

### bias
theme_set(theme_bw(base_size = 12))
coverage_year %>% 
  mutate(scenario = ifelse(sampling == "biased", "under-", sampling),
         scenario = ifelse(scenario == "over", "over-", scenario),
         scenario = paste(scenario, "\nsampling", sep = ""),
         scenario = factor(scenario, 
                           levels = c("unbiased\nsampling", "under-\nsampling", "over-\nsampling")
                           ),
         sample_size = paste("n_years = ", sample_size, sep = ""),
         sample_size = factor(sample_size,
                              levels = paste("n_years = ", c(3, 5, 10, 15), sep = "")
                              ),
         Year = factor(YEAR, levels = c(1:15))
         ) %>% 
  ggplot(aes(x = Year, y = bias, group = Year)) +
  geom_boxplot(fill = "#a6cee3") +
  scale_x_discrete(name = "# Year", breaks = c(3, seq(5, 15, 5))) +
  scale_y_continuous(name = "Bias (%)") +
  facet_grid(sample_size ~ scenario) +
  theme(legend.position = "top", 
        legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text = element_text(size = 12)
        )

ggsave(filename = paste("output/", "Figure5.png", sep = ""), 
       width = 15, height = 12, dpi = 300, units = "cm"
       )

theme_set(theme_bw(base_size = 12))
coverage_week %>% 
  mutate(scenario = ifelse(sampling == "biased", "under-", sampling),
         scenario = ifelse(scenario == "over", "over-", scenario),
         scenario = paste(scenario, "\nsampling", sep = ""),
         scenario = factor(scenario, 
                           levels = c("unbiased\nsampling", "under-\nsampling", "over-\nsampling")
                           ),
         sample_size = paste("n_years = ", sample_size, sep = ""),
         sample_size = factor(sample_size,
                              levels = paste("n_years = ", c(3, 5, 10, 15), sep = "")
                              ),
         Year = factor(YEAR, levels = c(1:15))
         ) %>% 
  group_by(scenario, week_id, sample_size) %>% 
  summarize(bias = mean(bias)) %>% 
  ggplot(aes(x = week_id, y = bias, group = week_id)) +
  geom_boxplot(fill = "#a6cee3") +
  # geom_smooth(method = "loess") +
  scale_x_continuous(name = "Week", breaks = seq(1, 53, 4)) +
  scale_y_continuous(name = "Bias (# bycatch event)",
                     breaks = seq(-15, 15, 5)
                     ) +
  # scale_color_viridis_d(name = "Sampling") +
  facet_grid(sample_size ~ scenario) +
  theme(legend.position = "top", 
        legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text.y = element_text(size = 10), 
        axis.text.x = element_text(size = 8, angle = 45)
        )
ggsave(filename = paste("output/", "Figure6.jpeg", sep = ""), 
       width = 15, height = 12, dpi = 300, units = "cm"
       )

### coverage and ci width
coverage_year %>% 
  group_by(sampling, sample_size, YEAR) %>% 
  summarize(bias = mean(bias),
            coverage = mean(coverage),
            width = mean(width)
            ) %>% 
  group_by(sampling, sample_size) %>% 
  summarize(bias = round(mean(bias), 1),
            coverage = 100 * round(mean(coverage), 3),
            ci_width = round(mean(width), 1)
            ) %>% 
  as.data.frame() %>% 
  # xtable()
  write.table(file = "output/multi_years_summary.txt",
              quote = FALSE, sep = "\t", row.names = FALSE,
              col.names = TRUE
              )
