##--------------------------------------------------------------------------------------------------------
## SCRIPT : simulation Common Dolphins - balanced sampling
##
## Authors : Matthieu Authier
## Last update : 2021-03-20
## R version 4.0.2 (2020-06-22) -- "Taking Off Again"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("lubridate", "skimr", "tidyverse", "rstan"), library, character.only = TRUE)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

rm(list = ls())

DataDir <- paste("data", sep = "/")
OutDir <- paste("output", sep = "/")

source(paste(DataDir, "simul_dolphin_multiyear.r", sep = "/"))

source(paste(DataDir, "MrP.r", sep = "/"))

## Compilation
M <- stan_model(model_code = MrP_code,
                model_name = "MrP model for bycatch estimation",
                verbose = TRUE
                )

### Modelling
n_chains <- 4
n_iter <- 1000
n_warm <- 500
n_thin <- 1

### monte carlo study
n_sim <- 1e2
set.seed(20200711)
seeds <- sample.int(1e6, size = n_sim, replace = FALSE)

# all_stanfit <- vector(mode = 'list', length = n_sim)
mrp <- theta <- NULL

for(i in 1:n_sim) {
  message(paste("Processing Simulations", i, sep = " "))
  dd <- ptm(repro_id = seeds[i], biased = TRUE, oversampling = TRUE)
  
  standata <- with(dd$bycatch_data %>% 
                     mutate(week_id = week(date)),
                   list(n_obs = nrow(dd$bycatch_data),
                        n_week = nrow(dd$sumstat),
                        n_vessel = length(unique(ptm_ID)),
                        WEEK = week_id,
                        VESSEL = as.numeric(factor(ptm_ID, levels = unique(ptm_ID))),
                        BYCATCH = n_bycatch,
                        N = n_OP,
                        prior_scale = log(2) / 10,
                        prior_location_intercept = 0,
                        prior_scale_intercept = 1.5,
                        n_op = dd$sumstat$n_op
                        )
                   )
  # Now model fitting but check first that there is data
  if(sum(standata$BYCATCH) != 0) {
    fit <- sampling(object = M,
                    data = standata, 
                    pars = c("bycatch_hat", "bycatch_tot"),
                    chains = n_chains, 
                    iter = n_iter, 
                    warmup = n_warm, 
                    thin = n_thin,
                    control = list(adapt_delta = 0.99, max_treedepth = 15)
                    )
    
    # check convergence
    ggsave(paste(OutDir, "/convergence/1_year_over_conv_", i, ".png", sep = ""), 
           plot = plot(fit, plotfun = 'rhat'), 
           width = 10, height = 10, units = "cm"
           )
    
    df <- rbind(t(apply(rstan::extract(fit, 'bycatch_hat')$bycatch_hat[, 1, ], 2, get_summary)),
                t(apply(rstan::extract(fit, 'bycatch_hat')$bycatch_hat[, 2, ], 2, get_summary))
                ) %>% 
      round(3) %>% 
      as.data.frame()
    names(df) <- c("theta", "lower", "upper")
    sumstat <- cbind(rbind(dd$sumstat, dd$sumstat), df) %>% 
      mutate(vessel = rep(0:1, each = n()/2),
             sim = i
             )
    mrp <- rbind(mrp, sumstat)
    
    res <- t(apply(rstan::extract(fit, 'bycatch_tot')$bycatch_tot, 2, get_summary))
    out <- data.frame(sim = rep(i, 4),
                      theta = c(dd$wgbyc$theta[2], dd$wgbyc$theta[2], res[, 1]),
                      lower = c(dd$wgbyc$lower_clopperpearson[2], dd$wgbyc$lower_boot[2], res[, 2]),
                      upper = c(dd$wgbyc$upper_clopperpearson[2], dd$wgbyc$upper_boot[2], res[, 3]),
                      method = c("Clopper-Pearson", "Bootstrap", "MrP", "MrP_vessel"),
                      N_OP = rep(dd$wgbyc$N_OP[2], 4),
                      N_bycatch = rep(dd$wgbyc$N_bycatch[2], 4),
                      OP_obs = rep(dd$wgbyc$N_OP[1], 4),
                      bycatch_obs = rep(dd$wgbyc$N_bycatch[1], 4)
                      )
    theta <- rbind(theta, out)
    
    # all_stanfit[[i]] <- fit
    
    rm(res, out, fit, dd, df, sumstat)
  }
}; rm(M, MrP_code, i, n_chains, n_iter, n_thin, n_warm)

gc()
save.image(paste(OutDir, "1_year_over.RData", sep = "/"), safe = TRUE)
