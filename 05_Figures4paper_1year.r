##--------------------------------------------------------------------------------------------------------
## SCRIPT : 1 year
##
## Authors : Matthieu Authier
## Last update : 2021-03-20
## R version 4.0.2 (2020-06-22) -- "Taking Off Again"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("lubridate", "skimr", "tidyverse", "rstan"), library, character.only = TRUE)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

rm(list = ls())

### unbiased sampling
load(paste("output", "1_year_unbiased.RData", sep = "/"))

### for paper
profile <- mrp %>% 
  mutate(scenario = "unbiased\nsampling")

### bias
N <- theta %>% 
  rename(Uncertainty = method) %>% 
  filter(Uncertainty != "MrP") %>% 
  mutate(bias = 100 * round((theta - N_bycatch) / N_bycatch, 3),
         inside = ifelse(theta > lower, 1, 0) * ifelse(theta < upper, 1, 0),
         method = ifelse(Uncertainty %in% c("Clopper-Pearson", "Bootstrap"),
                         "Design-based",
                         "Model-based"
                         ),
         Uncertainty = ifelse(Uncertainty %in% c("Clopper-Pearson", "Bootstrap"),
                              Uncertainty,
                              "Bayesian"
                              ),
         scenario = "unbiased\nsampling"
         )

### biased undersampling
load(paste("output", "1_year_biased.RData", sep = "/"))
rm(standata, seeds, accept_observer, dolphin, get_summary, ptm)

### for paper
profile <- rbind(profile,
                 mrp %>% 
                   mutate(scenario = "under-\nsampling")
                 )

### bias
N <- rbind(N,
           theta %>% 
             rename(Uncertainty = method) %>% 
             filter(Uncertainty != "MrP") %>% 
             mutate(bias = 100 * round((theta - N_bycatch) / N_bycatch, 3),
                    inside = ifelse(theta > lower, 1, 0) * ifelse(theta < upper, 1, 0),
                    method = ifelse(Uncertainty %in% c("Clopper-Pearson", "Bootstrap"),
                                    "Design-based",
                                    "Model-based"
                                    ),
                    Uncertainty = ifelse(Uncertainty %in% c("Clopper-Pearson", "Bootstrap"),
                                         Uncertainty,
                                         "Bayesian"
                                         ),
                    scenario = "under-\nsampling"
                    )
           )

### biased oversampling
load(paste("output", "1_year_over.RData", sep = "/"))
rm(standata, seeds, accept_observer, dolphin, get_summary, ptm)

### for paper
profile <- rbind(profile,
                 mrp %>% 
                   mutate(scenario = "over-\nsampling")
                 )

### bias
N <- rbind(N,
           theta %>% 
             rename(Uncertainty = method) %>% 
             filter(Uncertainty != "MrP") %>% 
             mutate(bias = 100 * round((theta - N_bycatch) / N_bycatch, 3),
                    inside = ifelse(theta > lower, 1, 0) * ifelse(theta < upper, 1, 0),
                    method = ifelse(Uncertainty %in% c("Clopper-Pearson", "Bootstrap"),
                                    "Design-based",
                                    "Model-based"
                                    ),
                    Uncertainty = ifelse(Uncertainty %in% c("Clopper-Pearson", "Bootstrap"),
                                         Uncertainty,
                                         "Bayesian"
                                         ),
                    scenario = "over-\nsampling"
                    )
           )

rm(theta, mrp)

theme_set(theme_bw(base_size = 12))
N %>% 
  mutate(scenario = factor(scenario, 
                           levels = c("unbiased\nsampling", "under-\nsampling", "over-\nsampling")
                           ),
         Uncertainty = ifelse(Uncertainty == "Clopper-Pearson", 
                              "\nClopper-Pearson", 
                              Uncertainty
                              ),
         Uncertainty = factor(Uncertainty,
                              levels = c("Bootstrap", "\nClopper-Pearson", "Bayesian")
                              )
         ) %>% 
  ggplot(aes(x = Uncertainty, y = bias, fill = method)) +
  geom_violin() +
  geom_hline(yintercept = 0, color = "tomato", linetype = "dashed") +
  scale_y_continuous(name = "Bias (%)") +
  facet_grid(~ scenario) +
  scale_fill_manual(values = c("#b2df8a", "#a6cee3")) +
  theme(legend.position = "top", 
        legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text.y = element_text(size = 10),
        axis.text.x = element_text(size = 8)
        )

ggsave(filename = paste("output", "Figure3.jpeg", sep = "/"), 
       width = 15, height = 12, units = "cm", dpi = 300
       )


theme_set(theme_bw(base_size = 12))
N %>% 
  mutate(scenario = factor(scenario, 
                           levels = c("unbiased\nsampling", "under-\nsampling", "over-\nsampling")
                           )
         ) %>% 
  ggplot(aes(x = N_bycatch, y = theta, color = method, group = Uncertainty)) +
  geom_abline(intercept = 0, slope = 1, color = "tomato", linetype = "dotted") +
  geom_point() +
  geom_smooth(method = "lm", se = FALSE) +
  scale_x_continuous(name = "Truth", breaks = seq(0, 500, 50)) +
  scale_y_continuous(name = "Estimate", breaks = seq(0, 1000, 100)) +
  facet_grid(~ scenario) +
  scale_color_manual(values = c("#b2df8a", "#a6cee3")) +
  # scale_color_brewer(name = "Method", type = "qual", palette = "Set2") +
  theme(legend.position = "top", 
        legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text.y = element_text(size = 10),
        axis.text.x = element_text(size = 8)
        )

ggsave(filename = paste("output", "Figure4.jpeg", sep = "/"), 
       width = 15, height = 12, units = "cm", dpi = 300
       )

### coverage
N %>% 
  mutate(ciwidth = 100 * round((upper - lower) / theta, 3),
         sample_size = 1
         ) %>% 
  group_by(Uncertainty, scenario, sample_size) %>% 
  summarize(bias = round(mean(bias), 1),
            coverage = 100 * round(mean(inside), 3),
            ci_width = round(mean(ciwidth), 1),
            n = round(mean(bycatch_obs), 0)
            ) %>% 
  as.data.frame() %>% 
  # xtable()
  write.table(file = "output/one_year_summary.txt",
              quote = FALSE, sep = "\t", row.names = FALSE,
              col.names = TRUE
              )

### look at the temporal pattern
theme_set(theme_bw(base_size = 12))
profile %>% 
  mutate(scenario = factor(scenario, 
                           levels = c("unbiased\nsampling", "under-\nsampling", "over-\nsampling")
                           ),
         theta = theta / n_op
         ) %>% 
  ggplot(aes(x = week_id, y = theta, group = sim)) +
  geom_line(alpha = 0.05) +
  scale_y_continuous(name = "Bycatch risk") +
  scale_x_continuous(name = "Week", breaks = seq(1, 53, 4)) +
  facet_grid(~ scenario) +
  theme(legend.position = "top", 
        legend.key.width = unit(2, "cm"),
        plot.title = element_text(lineheight = 0.8, face = "bold"), 
        axis.text.y = element_text(size = 10), 
        axis.text.x = element_text(size = 8, angle = 45)
        )

ggsave(filename = paste("output", "Figure5.jpeg", sep = "/"), 
       width = 15, height = 12, units = "cm", dpi = 300
       )
