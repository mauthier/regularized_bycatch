##--------------------------------------------------------------------------------------------------------
## SCRIPT : simulation Common Dolphins - results of balanced sampling
##
## Authors : Matthieu Authier
## Last update : 2021-03-29
## R version 4.0.4 (2021-02-15) -- "Lost Library Book"
## Copyright (C) 2020 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "viridisLite"), library, character.only = TRUE)

rm(list = ls())

summary_sim <- function(n_year, scenario) {
  load(paste("output/", n_year, "_year_", scenario, ".RData", sep = ""))

  ## bias
  theta <- theta %>%
    mutate(bias = 100 * round((theta - truth) / truth, 3),
           inside = ifelse(theta > lower, 1, 0) * ifelse(theta < upper, 1, 0)
           )

  theme_set(theme_bw())
  theta %>%
    ggplot(aes(x = factor(YEAR), y = bias)) +
    geom_violin() +
    scale_x_discrete(name = "# Year") +
    scale_y_continuous(name = "Bias (%)") +
    theme(legend.position = "top",
          legend.key.width = unit(2, "cm"),
          plot.title = element_text(lineheight = 0.8, face = "bold"),
          axis.text = element_text(size = 12)
          )
  ggsave(filename = paste("output/", n_year, "_year_", scenario, "_boxplot.png", sep = ""),
         width = 20, height = 15, units = "cm", dpi = 600
         )

  theme_set(theme_bw())
  theta %>%
    ggplot(aes(x = YEAR, y = bias)) +
    geom_line(aes(group = sim), color = "midnightblue", alpha = 0.3) +
    geom_smooth(method = "loess", color = "midnightblue") +
    scale_x_continuous(name = "# Year", breaks = 1:n_year) +
    scale_y_continuous(name = "Bias (%)") +
    theme(legend.position = "top",
          legend.key.width = unit(2, "cm"),
          plot.title = element_text(lineheight = 0.8, face = "bold"),
          axis.text = element_text(size = 12)
          )
  ggsave(filename = paste("output/", n_year, "_year_", scenario, "_spaghettiplot_year.png", sep = ""),
         width = 20, height = 15, units = "cm", dpi = 600
         )

  ## temporal pattern within a year
  mrp <- mrp %>%
    mutate(bias = theta - n_bycatch,
           inside = ifelse(theta > lower, 1, 0) * ifelse(theta < upper, 1, 0)
           )

  theme_set(theme_bw())
  mrp %>%
    ggplot(aes(x = week_id, y = bias)) +
    # geom_line(aes(group = sim), color = "midnightblue", alpha = 0.3) +
    geom_smooth(aes(color = YEAR, group = YEAR), method = "loess", se = FALSE) +
    scale_x_continuous(name = "# week", breaks = c(1, seq(4, 50, 4), 52)) +
    scale_y_continuous(name = "Bias (# bycatch)") +
    scale_color_gradientn(name = "# Year",
                          colors = viridisLite::viridis(256, option = "D"),
                          breaks = 1:n_year
                          ) +
    theme(legend.position = "top",
          legend.key.width = unit(2, "cm"),
          plot.title = element_text(lineheight = 0.8, face = "bold"),
          axis.text = element_text(size = 12)
          )
  ggsave(filename = paste("output/", n_year, "_year_", scenario, "_spaghettiplot_week.png", sep = ""),
         width = 20, height = 15, units = "cm", dpi = 600
         )

  ### tables
  coverage_year <- theta %>%
    # width as a percentage of point estimate
    mutate(width = 100 * round((upper - lower) / theta, 3)) %>%
    group_by(YEAR, sim) %>%
    summarize(bias = mean(bias),
              coverage = mean(inside),
              width = mean(width)
              ) %>%
    mutate(sample_size = n_year,
           sampling = scenario
           )

  coverage_week <- mrp %>%
    mutate(width = upper - lower) %>%
    group_by(YEAR, week_id, sim) %>%
    summarize(bias = mean(bias),
              coverage = mean(inside),
              width = mean(width),
              n = round(mean(n_bycatch), 0)
              ) %>%
    mutate(sample_size = n_year,
           sampling = scenario
           )
  ### wrap-up
  return(list(coverage_year = coverage_year, coverage_week = coverage_week))
}

coverage_year <- coverage_week <- NULL
for(i in c(3, 5, 10, 15)) {
  for(j in c("unbiased", "biased", "over")) {
    dd <- summary_sim(n_year = i, scenario = j)
    coverage_year <- rbind(coverage_year,
                           dd$coverage_year
                           )
    coverage_week <- rbind(coverage_week,
                           dd$coverage_week
                           )
    rm(dd)
  }
}; rm(i, j)

write.table(coverage_year,
            paste("output", "20210329_YEAR.txt", sep = "/"),
            quote = FALSE, sep = "\t", row.names = FALSE, col.names = TRUE
            )

write.table(coverage_week,
            paste("output", "20210329_WEEK.txt", sep = "/"),
            quote = FALSE, sep = "\t", row.names = FALSE, col.names = TRUE
            )

# coverage_year <- read.table(paste(OutDir, "20201212_YEAR.txt", sep = "/"),
#                             header = TRUE, 
#                             colClasses = c(rep("numeric", 5), "character")
#                             )
# 
# coverage_week <- read.table(paste(OutDir, "20201212_WEEK.txt", sep = "/"),
#                             header = TRUE, 
#                             colClasses = c(rep("numeric", 6), "character")
#                             )
# 
# ### bias
# theme_set(theme_bw())
# coverage_year %>% 
#   mutate(sampling = paste(sampling, "sampling", sep = " ")) %>% 
#   ggplot(aes(x = YEAR, y = bias, group = sample_size)) +
#   geom_smooth(method = "lm") +
#   scale_x_continuous(name = "# Year", breaks = c(3, seq(5, 15, 5))) +
#   scale_y_continuous(name = "Bias (%)") +
#   # scale_color_viridis_d(name = "Sampling") +
#   facet_grid(sample_size ~ sampling) +
#   theme(legend.position = "top", 
#         legend.key.width = unit(2, "cm"),
#         plot.title = element_text(lineheight = 0.8, face = "bold"), 
#         axis.text = element_text(size = 12)
#         )
# ggsave(filename = paste(OutDir, "/", "Bias_year.png", sep = ""), 
#        width = 20, height = 15, units = "cm", dpi = 600
#        )
# 
# theme_set(theme_bw())
# coverage_week %>% 
#   mutate(sampling = paste(sampling, "sampling", sep = " ")) %>% 
#   group_by(sampling, week_id, sample_size) %>% 
#   summarize(bias = mean(bias)) %>% 
#   ggplot(aes(x = week_id, y = bias, group = sample_size)) +
#   geom_smooth(method = "loess") +
#   scale_x_continuous(name = "# Year", breaks = c(1, seq(4, 50, 4), 52)) +
#   scale_y_continuous(name = "Bias (# bycatch)") +
#   # scale_color_viridis_d(name = "Sampling") +
#   facet_grid(sample_size ~ sampling) +
#   theme(legend.position = "top", 
#         legend.key.width = unit(2, "cm"),
#         plot.title = element_text(lineheight = 0.8, face = "bold"), 
#         axis.text = element_text(size = 12)
#         )
# ggsave(filename = paste(OutDir, "/", "Bias_week.png", sep = ""), 
#        width = 20, height = 15, units = "cm", dpi = 600
#        )
# 
# ### coverage
# theme_set(theme_bw())
# coverage_year %>% 
#   mutate(sampling = paste(sampling, "sampling", sep = " ")) %>% 
#   ggplot(aes(x = YEAR, y = coverage, group = sample_size)) +
#   geom_smooth(method = "lm") +
#   scale_x_continuous(name = "# Year", breaks = c(3, seq(5, 15, 5))) +
#   scale_y_continuous(name = "coverage (%)") +
#   # scale_color_viridis_d(name = "Sampling") +
#   facet_grid(sample_size ~ sampling) +
#   theme(legend.position = "top", 
#         legend.key.width = unit(2, "cm"),
#         plot.title = element_text(lineheight = 0.8, face = "bold"), 
#         axis.text = element_text(size = 12)
#         )
# ggsave(filename = paste(OutDir, "/", "Coverage_year.png", sep = ""), 
#        width = 20, height = 15, units = "cm", dpi = 600
#        )
# 
# theme_set(theme_bw())
# coverage_week %>% 
#   mutate(sampling = paste(sampling, "sampling", sep = " ")) %>% 
#   group_by(sampling, week_id, sample_size) %>% 
#   summarize(coverage = mean(coverage)) %>% 
#   ggplot(aes(x = week_id, y = coverage, group = sample_size)) +
#   geom_smooth(method = "loess") +
#   scale_x_continuous(name = "# Year", breaks = c(1, seq(4, 50, 4), 52)) +
#   scale_y_continuous(name = "Coverage (%)") +
#   # scale_color_viridis_d(name = "Sampling") +
#   facet_grid(sample_size ~ sampling) +
#   theme(legend.position = "top", 
#         legend.key.width = unit(2, "cm"),
#         plot.title = element_text(lineheight = 0.8, face = "bold"), 
#         axis.text = element_text(size = 12)
#         )
# ggsave(filename = paste(OutDir, "/", "Coverage_week.png", sep = ""), 
#        width = 20, height = 15, units = "cm", dpi = 600
#        )
# 
# ### width
# theme_set(theme_bw())
# coverage_year %>% 
#   mutate(sampling = paste(sampling, "sampling", sep = " ")) %>% 
#   ggplot(aes(x = YEAR, y = 100 * width, group = sample_size)) +
#   geom_smooth(method = "lm") +
#   geom_hline(yintercept = 100, color = "tomato", linetype = "dashed") +
#   scale_x_continuous(name = "# Year", breaks = c(3, seq(5, 15, 5))) +
#   scale_y_continuous(name = "CI width (%)") +
#   # scale_color_viridis_d(name = "Sampling") +
#   facet_grid(sample_size ~ sampling) +
#   theme(legend.position = "top", 
#         legend.key.width = unit(2, "cm"),
#         plot.title = element_text(lineheight = 0.8, face = "bold"), 
#         axis.text = element_text(size = 12)
#         )
# ggsave(filename = paste(OutDir, "/", "CIWidth_year.png", sep = ""), 
#        width = 20, height = 15, units = "cm", dpi = 600
#        )
# 
# theme_set(theme_bw())
# coverage_week %>% 
#   mutate(sampling = paste(sampling, "sampling", sep = " ")) %>% 
#   group_by(sampling, week_id, sample_size) %>% 
#   summarize(width = mean(width)) %>% 
#   ggplot(aes(x = week_id, y = width, group = sample_size)) +
#   geom_smooth(method = "loess") +
#   scale_x_continuous(name = "# Year", breaks = c(1, seq(4, 50, 4), 52)) +
#   scale_y_continuous(name = "CI width (# bycatch)") +
#   # scale_color_viridis_d(name = "Sampling") +
#   facet_grid(sample_size ~ sampling) +
#   theme(legend.position = "top", 
#         legend.key.width = unit(2, "cm"),
#         plot.title = element_text(lineheight = 0.8, face = "bold"), 
#         axis.text = element_text(size = 12)
#         )
# ggsave(filename = paste(OutDir, "/", "CIWidth_week.png", sep = ""), 
#        width = 20, height = 15, units = "cm", dpi = 600
#        )
